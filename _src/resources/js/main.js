
  /* Init */

function pageLoad(){

$('.flexslider.task-list').flexslider({
      animation: "slide",
      directionNav: false
    });

$('.flexslider').flexslider({
      animation: "slide",
      directionNav: false

    });

$('.accordion-content').each(function(){
  if ( $(this).children().length > 0 ) {
    $(this).parent().addClass('has-sub')
  }
})



$('.has-sub .accordion-title').click(function(){
  $('.accordion-content').slideUp('');
  if( $(this).hasClass('active')) {
    $(this).removeClass('active').next().slideUp();
  } else {
    $('.accordion-title').removeClass('active');
    $(this).addClass('active').next().slideDown();
  }
})





$('#accordion-widget').on('show.bs.collapse',function() {
  $('#distributors').fadeOut();
  $(this).parents('.col-sm-6').removeClass('col-sm-6');
  $('#closeAccordion').fadeIn();
})

$('#closeAccordion').click(function(){
  $('.panel-collapse.in').collapse('hide');
  $(this).closest('.col-xs-12').addClass('col-md-6')
  $('#distributors').fadeIn();
  $(this).fadeOut();



});


}





$(window).resize(function(){
  imagePadding();
  
})


$(document).ready(function(){
  pageLoad();
  
})




